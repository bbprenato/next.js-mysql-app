import useSWR, { trigger, mutate } from 'swr' //the trigger method does the call again with the cached data to make sure the component updates with the new data once changes are made. Whereas mutate does an update to the state
import {fetchLoggedUser} from '../redux/actions/users' 
import axios from 'axios'

const fetcher = async (url) => {
  return await axios.get(url).then(res => res.json())
}

const useUser = () => {
  const { user, error } = useSWR('/api/users/auth', fetchLoggedUser)
  console.log(`Data: ${user}`)

  return {
    noUser: !user,
    user: user ? user.data : null,
    isLoading: !error && !user,
    isError: error,
  }
}


export default useUser;
