const { Sequelize } = require('sequelize');
const dbConfig = require('../config/config')

const database = dbConfig.development;
console.log(`Using database for development: ${database.database}. Credentials for the user: ${database.username} at host: ${database.host}`)

const sequelize = new Sequelize(dbConfig.development);

module.exports = sequelize;