const Sequelize = require('sequelize');
const db = require('../database/database');

class Favorites extends Sequelize.Model {}
Favorites.init({
    name: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    amount: {
        type: Sequelize.INTEGER,
        defaultValue: 0
    },
}, {
    sequelize: db,
    modelName: 'favorite'
})

module.exports = Favorites;