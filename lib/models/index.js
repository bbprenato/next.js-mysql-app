const User = require('./User');
const Favorites = require('./Favorite');

Favorites.belongsTo(User);

module.exports = { User, Favorites };