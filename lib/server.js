const db = require("./database/database");
const next = require('next')
const express = require("express");
const server = express();
const passport = require("./passport/passport");
const session = require("express-session");
const Router = require("../pages/api");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const dev = process.env.NODE_ENV !== 'production';
const app = next({
    dev
});
const handle = app.getRequestHandler()

server.use(morgan("dev"));
server.use(cookieParser());

/****Passport configuration****/

/******* session() is used before passport.session() to ensure that the login session is restored in the correct order: *******/
server.use(
    session({
        secret: "cats",
        resave: true,
        saveUninitialized: true,
    })
);
server.use(morgan("tiny"));

/******** In order to support login sessions (with the unique cookie of the session), Passport will serialize and deserialize user instances to and from the session:  *******/
server.use(passport.initialize());
server.use(passport.session());

server.use(bodyParser.urlencoded({
    extended: false
}));
server.use(bodyParser.json());
server.use("/api", Router);

const port = parseInt(process.env.PORT, 10) || 3000;
app.prepare()
    .then(() => {
        /***** Syncronize db: *******/
        db.sync({
            logging: false,
            force: false
        }).then(() => {
            console.log('Database succesfully syncronized!')
        }).catch(e => console.log(e))

        server.get('/*', (req, res) => {
            return handle(req, res)
        })

        server.listen(port, (err) => {
            if (err) throw err
            console.log(`> Ready on http://localhost:${port}`)
        })
    })
    .catch((ex) => {
        console.error(ex.stack)
        process.exit(1)
    })


/***** Syncronize db:   *******/
// db.sync({ logging: false, force: false }).then(() => console.log('Database succesfully syncronized!')).catch(e => console.log(e))