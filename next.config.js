const path = require('path')
const envPath = path.resolve(process.cwd(), '.env.local')

require('dotenv').config({ path: envPath })

module.exports = {
    env: {
        API_URL: process.env.API_URL
    }
}