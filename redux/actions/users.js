import {
    REGISTER_SUCCESS,
    REGISTER_FAILS,
    LOGIN_SUCCESS,
    LOGOUT
} from '../constants';
// import URL from '../../config'
import axios from 'axios';

// const url = 'http://localhost:3000'

export const login = user => ({
    type: LOGIN_SUCCESS,
    user
})

const logout = user => ({
    type: LOGOUT,
    user
})

const register = user => ({
    type: REGISTER_SUCCESS,
    user
})

export const createUser = user =>
    dispatch =>
    axios.post('/api/users/create', user)
    .then(res => dispatch(register(res.data)))
    .catch(err => console.log(err))
    .finally(() => console.log('whatever you do here, handle the error properly with some custom component like 404 or sth'))


export const loginUser = user => {
    return dispatch => {
        axios.post('/api/users/login', user)
            .then(user => {
                dispatch(login(user.data));
                return user.data
            })
            .catch(err => console.log(err))
    }
}

export const logoutUser = () => {
    return function (dispatch) {
        return dispatch(logout())
    }
}