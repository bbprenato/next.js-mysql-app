import { REGISTER_SUCCESS, REGISTER_FAILS, LOGIN_SUCCESS, LOGOUT } from "../constants";

export const initialState = {
    user: {},
    isLoguedIn: false,
};

export default function userReducers (state = initialState, action) {
    switch (action.type) {
        case REGISTER_SUCCESS:
            return Object.assign({}, state, { user: action.user, isLoguedIn: true });
        case REGISTER_FAILS:
            return Object.assign({}, state, { user: {} })
        case LOGIN_SUCCESS:
            return Object.assign({}, state, { user: action.user, isLoguedIn: true })
        case LOGOUT:
            return Object.assign({}, state, { user: {}, isLoguedIn: false })
        default:
            return state;
    }
};
