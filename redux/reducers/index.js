import { combineReducers } from "redux";
import userReducers from "./userReducers";
// import favoritesReducers from 'path to reducer'

const reducers = combineReducers({
  userReducer: userReducers
  // favorites: favoritesReducers
});

export default reducers;