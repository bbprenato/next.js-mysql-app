/**************Users: ****************/
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILS = 'REGISTER_FAILS';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILS = 'LOGIN_FAILS';
export const LOGOUT = 'LOGOUT';