import { useMemo } from 'react'
import { createStore, applyMiddleware } from 'redux';
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import hardSet from 'redux-persist/lib/stateReconciler/hardSet'
import thunkMiddleware from 'redux-thunk';
import { createLogger } from "redux-logger";
import { composeWithDevTools } from 'redux-devtools-extension'

import rootReducer from './reducers';


let store

const rootPersistConfig = {
  key: 'primary',
  storage,
  stateReconciler: hardSet, //This will hard set incoming state. This can be desirable in some cases where persistReducer is nested deeper in your reducer tree, or if you do not rely on initialState in your reducer.
  whiteList: ['userReducer']
}

const persistedReducer = persistReducer(rootPersistConfig, rootReducer)

function initStore() {
  return createStore(
    persistedReducer,
    composeWithDevTools(applyMiddleware(createLogger(), thunkMiddleware))
    )
}

export const initializeStore = (preloadedState) => {
  let _store = store ?? initStore(preloadedState)

  // After navigating to a page with an initial Redux state, merge that state
  // with the current state in the store, and create a new store
  if (preloadedState && store) {
    _store = initStore({
      ...store.getState(),
      ...preloadedState,
    })
    // Reset the current store
    store = undefined
  }

  // For SSG or SSR always create a new store
  if (typeof window === 'undefined') return _store
  // Create the store once in the client
  if (!store) store = _store

  return _store
}

export function useStore(initialState) {
  const store = useMemo(() => initializeStore(initialState), [initialState])
  return store
}
