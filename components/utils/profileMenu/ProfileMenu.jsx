import React, { useState } from 'react';
import Link from 'next/link'
import Menu from '@material-ui/core/Menu';
import { withStyles } from '@material-ui/core/styles'
import MenuItem from '@material-ui/core/MenuItem';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Tooltip from '@material-ui/core/Tooltip'
import IconButton from '@material-ui/core/IconButton';
import styles from './profileIcon.module.css'

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: 'rgba(180, 180, 180)',
    color: 'rgb(255, 255, 255)',
    // boxShadow: theme.shadows[1],
    fontSize: 13,
  },
}))(Tooltip);


const ProfileMenu = () => {

  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className={styles.profileIcon}>
      <LightTooltip title="Profile" leaveDelay={200}>
        <IconButton
          onClick={handleClick}
          component='default'
          edge="end"
          aria-label="account of current user"
          aria-haspopup="true"
        >
          <AccountCircle />
        </IconButton>
      </LightTooltip>
      <Menu
        id="profile-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <Link href='/user/register'>
          <MenuItem>Register</MenuItem>
        </Link>
        <MenuItem>Login</MenuItem>
        <MenuItem>Logout</MenuItem>
      </Menu>
    </div>
  );
}

export default ProfileMenu;
