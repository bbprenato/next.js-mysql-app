import React, { forwardRef } from 'react';
import styles from './customLink.module.css';

const CustomLink = forwardRef(({ onClick, href, content }, ref) => {
        return (
            <a className={styles.linkElement} href={href} onClick={onClick} ref={ref} content={content}>
                {content}
            </a>
        )
})

export default CustomLink;