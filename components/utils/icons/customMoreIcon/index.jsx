import styles from './customMoreIcon.module.css';

const CustomMoreIcon = () => {
    return (
        <>
            <div className={styles.moreIconContainer}>
                <div className={styles.threeDotsContainer}>
                    <div className={styles.moreIcon}></div>
                </div>
            </div>
            <div className={styles.square}></div>
        </>
    )
};

export default CustomMoreIcon;