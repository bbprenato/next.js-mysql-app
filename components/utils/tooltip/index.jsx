import { withStyles } from '@material-ui/core/styles'
import Tooltip from '@material-ui/core/Tooltip'

const LightTooltip = withStyles((theme) => ({
    tooltip: {
        backgroundColor: 'rgba(90, 90, 90)',
        color: 'rgb(255, 255, 255)',
        boxShadow: theme.shadows[1],
        fontSize: 13,
    },
}))(Tooltip);

export default LightTooltip