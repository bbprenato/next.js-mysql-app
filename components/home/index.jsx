import React from 'react';
import styles from './home.module.css';


const Home = () => {
    return (
        <>
            <h2 className={styles.title}>Here Home content</h2>
            <p>In the near future, this home page will be properly styled, SWR module will be independent from any page or component and it will fetch all necessary data from either APIs, the Database or both</p>
        </>
    )
}


export default Home;