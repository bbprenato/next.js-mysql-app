import React from 'react'
import styles from './footer.module.css'

const Footer = () => {

  return (
    <>
      <footer className={styles.footerSolid}>
        <div className={styles.footerPresenter}>
          <span className={styles.attribution}>A project by Renato Yanez</span>
        </div>
      </footer>
    </>
  )
}

export default Footer
