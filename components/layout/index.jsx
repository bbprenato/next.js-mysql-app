import React from 'react'
import NavBar from '../navbar'
import Footer from '../footer'
import styles from './layout.module.css'

export const siteTitle = 'Next.js Sample Website'

export default function Layout({ children }) {

  return (
    <>
      <NavBar />
      <div className={styles.container}>
        {children}
      </div>
      <Footer />
    </>
  )
}

