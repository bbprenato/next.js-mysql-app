import React, { useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import Head from 'next/head'
import { createUser } from '../../../redux/actions/users'
import { Avatar, Button, TextField, FormControlLabel, Checkbox, Link, Grid, Typography, Container } from '@material-ui/core';
import SendIcon from '@material-ui/icons/Send';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { makeStyles } from '@material-ui/core/styles';
import styles from './register.module.css';
import URL from '../../../config'

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    // backgroundColor: '#000000',
    // color: '#ffffff'
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: '#000000',
    color: '#ffffff'
  },
}));

const Register = () => {


  const userData = {
    first_name: '',
    last_name: '',
    email: '',
    username: '',
    password: ''
  }

  const [data, setData] = useState(userData);

  const [successful, setSuccessful] = useState(false);

  const dispatch = useDispatch();

  const handleInputChange = event => {
    // console.log({ name: event.target.name, value: event.target.value})
    setData({
      ...data,
      [event.target.name]: event.target.value
    })
  }
  const handleSubmit = async (e) => {
    e.preventDefault();
    // setLoading(true);

    dispatch(createUser(data))
      .then(() => {
        setSuccessful(true);
      })
      .catch(() => {
        setSuccessful(false);
      });

    // try {
    //   const { data, status } = await UserAPI.register(
    //     username,
    //     email,
    //     password
    //   );
    //   if (status !== 200 && data?.errors) {
    //     setErrors(data.errors);
    //   }
    //   if (data?.user) {
    //     window.localStorage.setItem("user", JSON.stringify(data.user));
    //     mutate("user", data.user);
    //     Router.push("/");
    //   }
    // } catch (error) {
    //   console.error(error);
    // } finally {
    //   setLoading(false);
    // }
  };

  // console.log('Successful: ', successful)
  /** In the future, you'll need the next function to validate that the user has filled all the inputs. Do it when you have full control of those inputs (controlled state)**/

  // const validateUser = (user) => {
  //   return !(
  //     user.name !== "" && user.email !== ""
  //   );
  // };

  const classes = useStyles();

  return (
    <>
      <Head><title>Sign Up for Next.js App</title></Head>
      <Container component="main" maxWidth="xs">
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <form className={classes.form} noValidate onSubmit={handleSubmit}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  onChange={handleInputChange}
                  required
                  id="first_name"
                  name="first_name"
                  label="First name"
                  fullWidth
                  // autoComplete="given-name"
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  onChange={handleInputChange}
                  required
                  fullWidth
                  id="last_name"
                  label="Last Name"
                  name="last_name"
                  // autoComplete="lname"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  onChange={handleInputChange}
                  required
                  fullWidth
                  name="username"
                  label="Username of your preference"
                  type="username"
                  id="username"
                  // autoComplete="username"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  onChange={handleInputChange}
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  // autoComplete="email"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  onChange={handleInputChange}
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  // autoComplete="current-password"
                />
              </Grid>
              <Grid item xs={12}>
                <FormControlLabel
                  control={<Checkbox value="allowExtraEmails" color="primary" />}
                  label="I have read terms and conditions and accept them."
                />
              </Grid>
            </Grid>
            <Button
              // disabled={validateUser(user)}
              onClick={handleSubmit}
              disableElevation
              type="submit"
              fullWidth
              variant="contained"
              // color="primary"
              className={classes.submit}
              endIcon={<SendIcon></SendIcon>}
            >
              Submit
      </Button>
            <Grid container justify="flex-end">
              <Grid item>
                <Link href="#" variant="body2">
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      </Container>

    </>
  )
}

export default Register;



// function Copyright() {
//   return (
//     <Typography variant="body2" color="textSecondary" align="center">
//       {'Copyright © '}
//       <Link color="inherit" href="https://material-ui.com/">
//         Your Website
//       </Link>{' '}
//       {new Date().getFullYear()}
//       {'.'}
//     </Typography>
//   );
// }
