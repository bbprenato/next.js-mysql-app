import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import { withStyles } from '@material-ui/core/styles'
import FavoriteIcon from '@material-ui/icons/Favorite'
import Tooltip from '@material-ui/core/Tooltip'
import styles from './favoriteIcon.module.css';

const LightTooltip = withStyles((theme) => ({
    tooltip: {
        backgroundColor: 'rgba(90, 90, 90)',
        color: 'rgb(255, 255, 255)',
        boxShadow: theme.shadows[1],
        fontSize: 13,
    },
}))(Tooltip);

const CustomFavoriteIcon = () => {
    return (
        <div className={styles.FavoriteIcon}>
            <LightTooltip title="Favorites">
                <IconButton>
                    <Badge badgeContent={null}>
                        <FavoriteIcon id='favorite' />
                    </Badge>
                </IconButton>
            </LightTooltip>
        </div>
    )
}

export default CustomFavoriteIcon;