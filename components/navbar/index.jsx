import React from 'react';
import { useSelector } from 'react-redux'
import { Typography, AppBar, Toolbar, Tooltip, useScrollTrigger } from '@material-ui/core';
import Search from './search'
import CustomFavoriteIcon from './favoriteIcon/FavoriteIcon'
// import ProfileMenu from './profileMenu/ProfileMenu'
import CustomProfileMenu from './customProfileMenu'
import styles from './navbar.module.css'

const ElevationScroll = props => {

  const { children } = props;

  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: undefined,
  });

  return React.cloneElement(children, {
    elevation: trigger ? 2 : 1,
    color: trigger ? 'default' : 'transparent'
  });
}


const NavBar = props => {

  const user = useSelector(state => state.user?.username)
  console.log('user at navbar: ', user)

  return (
    <div>
      <ElevationScroll {...props}>
        <AppBar color='default'>
          <Toolbar className={styles.toolBar}>

            <Tooltip title='Home'>
              {user !== undefined ? (<Typography>Home of {user}</Typography>)
              : (<Typography>Home</Typography>)
            }
            </Tooltip>

            <Tooltip title="Search">
              <Search></Search>
            </Tooltip>

            <div className={styles.barIcons}>
              <CustomFavoriteIcon />
              <CustomProfileMenu />
            </div>

          </Toolbar>
        </AppBar>
      </ElevationScroll>
      {/* Down here the Toolbar component is presented:  */}
      <Toolbar />
    </div>
  );
}

export default NavBar;




















// import React, { useState, useEffect } from 'react';
// import { makeStyles } from '@material-ui/core/styles';
// import AppBar from '@material-ui/core/AppBar';
// import Toolbar from '@material-ui/core/Toolbar';
// import IconButton from '@material-ui/core/IconButton';
// import Badge from '@material-ui/core/Badge';
// import AccountCircle from '@material-ui/icons/AccountCircle';
// import FavoriteIcon from '@material-ui/icons/Favorite';
// import Tooltip from '@material-ui/core/Tooltip';
// import ProfileMenu from './ProfileMenu'
// import Search from './search'


// const useStyles = makeStyles((theme) => ({
//   grow: {
//     flexGrow: 1,
//   },
//   leftContent: {
//     marginLeft: theme.spacing(2),
//   },
//   title: {
//     display: 'none',
//     [theme.breakpoints.up('sm')]: {
//       display: 'block',
//     },
//   },
//   navigation: {},
//   toggleDrawer: {},
// }));

// const NavBar = () => {

//   const [anchorEl, setAnchorEl] = React.useState(null);

//   const handleClick = (event) => {
//     setAnchorEl(event.currentTarget);
//   };

//   const handleClose = () => {
//     setAnchorEl(null);
//   };

//   const classes = useStyles();

//   const menuId = 'primary-search-account-menu';

//   return (
//     <div className={classes.grow}>
//       <AppBar position="relative" color='transparent'>
//         <Toolbar className={classes.leftContent}>
//           <Tooltip title="Home" arrow>
//             <h3 /*className={classes.title}*/>
//               Home
//           </h3>
//           </Tooltip>
//           <Search />
//           {/* use bellow lines to display the username on the navbar: */}
//           {/* {user !== undefined ? (<Typography className={classes.userDisplay} variant="subtitle1" noWrap gutterBottom>
//             {user}
//           </Typography>) : null} */}
//           <div>
//             {/* Favorite icon starts here. In the near future, move this to another file and import it here so this entire component is more readible by another developer or by yourself... */}
//             <Tooltip title="Favorites" arrow>
//               <IconButton>
//                 <Badge badgeContent={null}>
//                   <FavoriteIcon id='favorite' />
//                 </Badge>
//               </IconButton>
//             </Tooltip>
//             {/* ...Favorite icon ends here */}
//             {/* Profile icon starts here. In the near future, move this to another file and import it here so this entire component is more readible by another developer or by yourself... */}
//             <Tooltip title="Profile" interactive="true" >
//               <IconButton
//                 component='default'
//                 onClick={handleClick}
//                 anchorEl={anchorEl}
//                 open={Boolean(anchorEl)}
//                 onClose={handleClose}
//               >
//                 <ProfileMenu />
//               </IconButton>
//             </Tooltip>
//             {/* ...Profile icon ends here */}
//           </div>
//         </Toolbar>
//       </AppBar>
//       {/* <AppBarCollapse /> */}
//     </div>
//   );
// }

// export default NavBar;