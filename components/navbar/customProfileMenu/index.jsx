import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import Link from 'next/link'
import { logoutUser } from '../../../redux/actions/users'
import AccountCircle from '@material-ui/icons/AccountCircle';
import IconButton from '@material-ui/core/IconButton';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import CustomLink from '../../utils/customLink';
import LightTooltip from '../../utils/tooltip';

import styles from './customProfileMenu.module.css';

/**** Here some arrays with the dropdown content... In case you need it ***/
const userContent = [{ id: 1, key: 'My Profile' }, { id: 2, key: 'Edit Profile' }, { id: 3, key: 'Logout' }]
const noUserContent = [{ id: 1, key: 'Register' }, { id: 2, key: 'Login' }, { id: 3, key: 'DoSomething' }]


const DropdownMenu = ({ onClick }) => {

    const user = useSelector(state => state.userReducer.isLoguedIn)
    const dispatch = useDispatch();
    const handleLogout = e => {
        e.preventDefault();
        dispatch(logoutUser())
    }

    return (
        <div className={styles.dropdownContent}>
            {user ? (
                <>
                    <div className={styles.linkContainer}>
                        <Link href={`/user/${userContent[0].key.replace(/\s/g, '')}`} passHref>
                            <CustomLink content={userContent[0].key} onClick={onClick} />
                        </Link>
                    </div>

                    <div className={styles.linkContainer}>
                        <Link href={`/user/${userContent[1].key.replace(/\s/g, '')}`} passHref>
                            <CustomLink content={userContent[1].key} onClick={onClick} />
                        </Link>
                    </div>

                    <div onClick={handleLogout} className={styles.linkContainer} >
                        <p className={styles.linkElement}>Logout</p>
                    </div>
                </>

            ) : (
                    noUserContent.map(user => (
                        <div key={user.id} className={styles.linkContainer}>
                            <Link href={`/user/${user.key.replace(/\s/g, '')}`} passHref>
                                <CustomLink content={user.key} onClick={onClick} />
                            </Link>
                        </div>
                    )))}
        </div>
    )
}

const CustomProfileMenu = () => {

    const [dropdown, setDropdown] = useState(false);

    const handleClick = () => {
        setDropdown(!Boolean(dropdown))
    };

    const handleClickAway = () => {
        setDropdown(false);
    };

    return (
        <ClickAwayListener onClickAway={handleClickAway}>
            <div className={styles.customProfileMenu}>
                <LightTooltip title="Profile">
                    <IconButton
                        onClick={handleClick}
                        component='default'
                        edge="end"
                        aria-label="account of current user"
                        aria-haspopup="true"
                    >
                        <AccountCircle />
                    </IconButton>
                </LightTooltip>
                {dropdown ? <DropdownMenu onClick={handleClick} /> : null}
            </div>
        </ClickAwayListener>
    )
};

export default CustomProfileMenu;