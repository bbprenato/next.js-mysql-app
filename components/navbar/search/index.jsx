import React from 'react'
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import styles from './search.module.css'

/*
React.forwardRef creates a React component that forwards the ref attribute it receives to another component below in the tree.

React.forwardRef accepts a rendering function as an argument. React will call this function with props and ref as two arguments. This function should return a React node.

In this example, React passes a ref given to <div ref={ref}> element as a second argument to the rendering function inside the React.forwardRef call. This rendering function passes the ref to the <div ref={ref}> element.

As a result, after React attaches the ref, ref.current will point directly to the <div> DOM element instance.
*/

const Search = React.forwardRef((_, ref) => {

    return (
        <div ref={ref} className={styles.search}>
            <div className={styles.searchIcon}>
                <SearchIcon />
            </div>
            <form onSubmit={null}>
                <InputBase
                    onChange={null}
                    placeholder="Search…"
                    className={styles.inputInput}
                    inputProps={{ 'aria-label': 'search' }}
                />
            </form>
        </div>
    )
})

export default Search;