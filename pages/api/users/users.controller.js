const models = require("../../../lib/models");

class UsersController {
  async createUser(newUser) {
    const { User } = models
    const created = await User.create(newUser);
    return created
  }
}

module.exports = UsersController;