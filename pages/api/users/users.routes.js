const express = require("express");
const passport = require("../../../lib/passport/passport");
const UsersController = require("./users.controller");
const User = require('../../../lib/models/User')

const router = express.Router();
const controller = new UsersController();

router.post("/create", async (req, res, next) => {
  try {
    const user = req.body;
    const response = await controller.createUser(user);
    res.status(200).json(response);
  } catch (error) {
    next(error);
  }
});

router.post("/login", passport.authenticate("local"), (req, res) => {
  const userData = req.user.dataValues
  const { password, salt, ...user } = userData //Filters properties "password" and 'salt' to not send them to the client
  res.send(user)
});

module.exports = router;