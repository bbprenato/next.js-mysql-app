import React from 'react'
import Head from 'next/head'
import Container from '../components/container'
import Home from '../components/home'
import { useSelector } from 'react-redux'

const IndexPage = () => {
  const siteTitle = 'Next.js Sample Website'

  const userReducer = useSelector((state) => state.userReducer.user)
  console.log('STATE EN INDEX: ', userReducer)
  // const [isLoading, setIsLoading] = useState(false)

  // if (!isLoading) {
  //   return (
  //     <>
  //       <Head>
  //         <title>{siteTitle}</title>
  //       </Head>
  //       <Container>
  //         <h1>Loading...</h1>
  //       </Container>
  //     </>
  //   )
  // }

  return (
    <>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <Container>
        <Home />
      </Container>
    </>
  )
}

export default IndexPage;