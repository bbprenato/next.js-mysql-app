/*
This is a Global CSS file. It's useful for when you want styles to be loaded by every page

This App component is the top-level component which will be common across all the different pages. You can use this App component to keep state when navigating between pages, for example.

In Next.js, you can add global CSS files by importing them from pages/_app.js. You cannot import global CSS anywhere else.

The reason that global CSS can't be imported outside of pages/_app.js is that global CSS affects all elements on the page.

If you were to navigate from the homepage to the /posts/first-post page, global styles from the homepage would affect /posts/first-post unintentionally.
*/

import React, { useEffect } from 'react'
import Head from 'next/head'
import Layout from '../components/layout'
import { Provider } from 'react-redux';
import { useStore } from '../redux';
import { persistStore } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react'
import { CssBaseline } from '@material-ui/core';
import { StylesProvider } from '@material-ui/core/styles';

/*
By default, the styles are injected last in the <head> element of the page.
As a result, they gain more specificity than any other style sheet.
If you want to override Material-UI's styles, set StylesProvider's injectFirst prop.
 */
import '../styles/global.css'

if (typeof window !== "undefined") {
  require("lazysizes/plugins/attrchange/ls.attrchange.js");
  require("lazysizes/plugins/respimg/ls.respimg.js");
  require("lazysizes");
}

export default function App({ Component, pageProps }) {
  const store = useStore(pageProps.initialReduxState);
  const persistor = persistStore(store, {}, function () {
    persistor.persist()
  })

  console.log('PAGE PROPS AT _APP: ', pageProps)

  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    <>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1"
        />
      </Head>
      <Provider store={store}>
        <StylesProvider injectFirst={true}>
          {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
          <CssBaseline />
          <Layout>
            <PersistGate loading={null} persistor={persistor}>
              <Component {...pageProps} />
            </PersistGate>
          </Layout>
        </StylesProvider>
      </Provider>
    </>
  )
};
