import Head from "next/head";
import React from "react";

import RegisterForm from '../../components/users/registerForm';

const Register = () => (
    <>
        <Head>
            <title>register</title>
            <meta name="description" content="Please register before login" />
        </Head>

        <RegisterForm />

    </>
);

export default Register;