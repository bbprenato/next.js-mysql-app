import Head from "next/head";
import React from "react";

const EditProfile = () => (
    <>
        <Head>
            <title>Edit profile</title>
            <meta name="description" content="Edit user's profile" />
        </Head>

        <h1>Here you can edit your profile...</h1>
        <h2>Is this page necessary? I don't know</h2>

    </>
);

export default EditProfile;