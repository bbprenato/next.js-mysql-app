import Head from "next/head";
import React from "react";

const Profile = () => (
    <>
        <Head>
            <title>Profile</title>
            <meta name="description" content="User's profile" />
        </Head>

        <h1>User's profile. Render here your profile component... (When you build it)</h1>

    </>
);

export default Profile;