import Head from "next/head";
import React from "react";

import LoginForm from '../../components/users/loginForm';

const Login = () => (
    <>
        <Head>
            <title>login</title>
            <meta name="description" content="Login to your account" />
        </Head>

        <LoginForm />

    </>
);

export default Login;