module.exports = {
  URL: 'http://localhost:3000',
  API_URL: 'http://localhost:3000/api',
  API: 'https://reqres.in/api/users?page=',
  NASA_API: {
    URL: 'https://api.nasa.gov/',
    KEY: 'z8Ni80CssIa2D1MdHx5bcGrlWb09EGRcdTnN8jji'
  },
  UNSPLASH_API: 'https://api.unsplash.com/'
};